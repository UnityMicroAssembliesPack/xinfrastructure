﻿using UnityEngine;

public static class XInfrastructure
{
    static public T_ManagerType getManager<T_ManagerType>()
        where T_ManagerType : XManager
    {
        XManagersHolder theManagersHolder = getManagersHolder();
        var theManager = theManagersHolder.GetComponent<T_ManagerType>();
        if (!theManager) {
            theManager = theManagersHolder.gameObject.AddComponent<T_ManagerType>();
        }

        return theManager;
    }

    static private XManagersHolder getManagersHolder() {
        var theManagersHolder = GameObject.FindObjectOfType<XManagersHolder>();
        if (!theManagersHolder) {
            GameObject theManagersHolderGameObject = new GameObject();
            theManagersHolderGameObject.name =  "XManagersHolder";
            theManagersHolder = theManagersHolderGameObject.AddComponent<XManagersHolder>();
        }
        return theManagersHolder;
    }
}
